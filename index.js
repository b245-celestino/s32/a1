const http = require("http");

let port = 4000;

const server = http.createServer((request, response) => {
  if (request.url == "/" && request.method === "GET") {
    response.writeHead(200, { "Content-type": "text/plain" });
    response.end("Welcome to our booking system");
  } else if (request.url == "/profile" && request.method === "GET") {
    response.writeHead(200, { "Content-type": "text/plain" });
    response.end("Welcome to your profile");
  } else if (request.url == "/courses" && request.method === "GET") {
    response.writeHead(200, { "Content-type": "text/plain" });
    response.end("Here's our courses available");
  } else if (request.url === "/addCourse" && request.method === "POST") {
    response.writeHead(200, { "Content-type": "text/plain" });
    response.end("Add course to our response");
  } else if (request.url === "/updateCourse" && request.method === "PUT") {
    response.writeHead(200, { "Content-type": "text/plain" });
    response.end("Update a course to our resources");
  } else if (request.url === "/archiveCourse" && request.method === "DELETE") {
    response.writeHead(200, { "Content-type": "text/plain" });
    response.end("Archive courses to our resources");
  }
});

server.listen(port);

console.log(`Server is running at port ${port}! `);
